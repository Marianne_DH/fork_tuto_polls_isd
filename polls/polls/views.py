""" Cornice services.
"""
from cornice import Service


hello = Service(name='hello', path='/', description="Simplest app")

hello2 = Service(name='hello', path='/hello2/', description="Simplest app")


@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}


@hello2.get()
def affichetoto(request):
    return {'Hello': 'Marianne2'}


@hello2.post()
def affichetoto2(request):
    return {'Hello': 'Marianne est en formation'}